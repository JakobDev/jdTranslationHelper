#!/usr/bin/env python3
from jdTranslationHelper import jdTranslationHelper
import locale
import os


def makeFiles(directory, first, second):
    localFile = open(os.path.join(directory, first), "w")
    localFile.write("testlocal=This is a Test\n")
    localFile.write("hello=World")
    localFile.close()
    defaultFile = open(os.path.join(directory, second), "a")
    defaultFile.write("hello=123\n")
    defaultFile.write("modul=jdTranslationHelper=Test123")
    defaultFile.close()


def doTest(translations):
    assert translations.translate("testlocal") == "This is a Test"
    assert translations.translate("hello") == "World"
    assert translations.translate("modul") == "jdTranslationHelper=Test123"
    assert translations.translate("noTranslation") == "noTranslation"
    assert translations.translate("noTranslation", default="Hello testpy") == "Hello testpy"


def test_normalUsage(tmpdir):
    makeFiles(str(tmpdir), locale.getlocale()[0] + ".lang", "en_GB.lang")
    translations = jdTranslationHelper()
    translations.loadDirectory(str(tmpdir))
    doTest(translations)


def test_arguments(tmpdir):
    makeFiles(str(tmpdir), "selected.lang", "default.lang")
    translations = jdTranslationHelper(lang="selected", defaultLanguage="default")
    translations.loadDirectory(str(tmpdir))


def test_noDefaultLang(tmpdir):
    makeFiles(str(tmpdir), "selected.lang", "default.lang")
    translations = jdTranslationHelper(lang="noSelected", defaultLanguage="default")
    translations.loadDirectory(str(tmpdir))
    assert translations.translate("modul") == "jdTranslationHelper=Test123"
    assert translations.translate("hello") == "123"
    assert translations.translate("testlocal") == "testlocal"
